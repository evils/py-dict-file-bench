#!/usr/bin/env python3
""" cProfile benchmarks for each lexer component. Counts are tailored for a reasonable running time. """

from cProfile import Profile
from io import StringIO
import pstats
import sys
import time

class TestRunner:
    """ Measures and prints the execution time for a given function and test arguments. """

    def __init__(self) -> None:
        self._stats = []

    def clear(self) -> None:
        self._stats.clear()

    def _on(self) -> float:
        return time.time()

    def _off(self, start_time:float) -> None:
        self._stats.append(time.time() - start_time)

    def run(self, func, test_data) -> None:
        """ Evaluate a function under a timer.
            <func>      - Function to profile.
            <test_data> - Iterable of tuples containing positional test arguments, one tuple per test. """
        timer = self._on()
        for t in test_data:
            func(*t)
        self._off(timer)

    def format_best(self) -> str:
        return f"Raw execution time: {min(self._stats):.3f}s\n"


class ProfileTestRunner(TestRunner):
    """ Measures and prints fine details of execution timing for a given function and test arguments. """

    def __init__(self, *, max_lines=50, strip_to_level=1) -> None:
        """ <max_lines> - maximum number of methods to print profiles on.
            <strip_to_level> - how many directory levels do we leave in each file listing?
                               None: C:\\ProgramData\\Anaconda3\\lib\\test.py
                               0: test.py
                               1: lib\\test.py """
        super().__init__()
        self._max_lines = max_lines
        self._strip_to_level = strip_to_level

    def _on(self) -> Profile:
        pr = Profile()
        pr.enable()
        return pr

    def _off(self, pr:Profile) -> None:
        pr.disable()
        pr.create_stats()
        self._stats.append(pr)

    def format_best(self) -> str:
        """ Format a string with the execution time for each method during the quickest run. """
        colwidths = [12, 7, None, 7, None]
        colsep = "   "
        best_pr = min(self._stats, key=lambda p: max(s[3] for s in p.stats.values()))
        s_buf = StringIO()
        ps = pstats.Stats(best_pr, stream=s_buf).sort_stats('cumulative')
        ps.print_stats(self._max_lines)
        sections = []
        for line in s_buf.getvalue().splitlines()[5:]:
            if line.strip():
                *fields, path = line.split(maxsplit=5)
                if self._strip_to_level is not None:
                    path_segments = path.split('\\')
                    stripped_segments = path_segments[-self._strip_to_level-1:]
                    path = "\\".join(stripped_segments)
                for f, w in zip(fields, colwidths):
                    if w is not None:
                        sections += [f.rjust(w), colsep]
                sections += [path, '\n']
        return "".join(sections)


def main(func, *args, n=10):
    profiler = ProfileTestRunner()
    profiler.run(func, [args]*n)
    print(profiler.format_best())


if __name__ == '__main__':
    toml_file = './plover_main.toml'
    json_file = './plover_main.json'
    yaml_file = './plover_main.yaml'

    calls = 1;

    def json_import_and_load():
      import json
      json.load(open(json_file))
    def simple_json_import_and_load():
      import simplejson as json
      json.load(open(json_file))
    def rtoml_import_and_load():
      import rtoml as toml
      toml.load(open(toml_file))
    def pytomlpp_import_and_load():
      import pytomlpp as toml
      toml.loads(open(toml_file).read())
    def tomli_import_and_load():
      import tomli as toml
      toml.loads(open(toml_file).read())
    def toml_import_and_load():
      import toml
      toml.load(toml_file)
    def yaml_import_and_load():
      import yaml
      yaml.load(open(yaml_file), Loader=yaml.CLoader)
    def ruamel_yaml_import_and_load():
      # from the plover-yaml-dictionary plugin
      import ruamel.yaml
      yaml = ruamel.yaml.YAML(typ='safe')
      yaml.load(open(yaml_file))

    print("native JSON benchmark")
    main(json_import_and_load, n=calls)
    print("simpleJSON benchmark")
    main(simple_json_import_and_load, n=calls)
    print("RTOML benchmark")
    main(rtoml_import_and_load, n=calls)
    print("tomli benchmark")
    main(tomli_import_and_load, n=calls)
    print("pyTOMLpp benchmark")
    main(pytomlpp_import_and_load, n=calls)
    print("python TOML benchmark")
    main(toml_import_and_load, n=calls)
    print("pyYAML benchmark")
    main(yaml_import_and_load, n=calls)
    print("ruamel YAML benchmark")
    main(ruamel_yaml_import_and_load, n=calls)
