{ pkgs ? import <nixpkgs> { } }:

with pkgs;

mkShell {

  nativeBuildInputs = with python3.pkgs; [
    simplejson
    toml
    pytomlpp
    rtoml
    tomli
    pyyaml
    ruamel_yaml
  ];

}
